import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';
import { Fighter } from './models/fighter.model';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement: HTMLElement = <HTMLElement>document.getElementById('root');
  static loadingElement: HTMLElement = <HTMLElement>document.getElementById('loading-overlay');

  async startApp(): Promise<void> {
    try {
      App.loadingElement.style.visibility = 'visible';

      const fighters: Partial<Fighter>[] = await fighterService.getFighters();
      const fightersElement: HTMLDivElement = createFighters(fighters);

      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;
