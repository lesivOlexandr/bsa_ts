import { controls } from '../../constants/controls';
import { Fighter } from '../models/fighter.model';

const SECOND: number = 1000;

export async function fight(firstFighter: Fighter, secondFighter: Fighter): Promise<Fighter> {
  const firstHealthIndicator: HTMLElement = <HTMLElement>document.getElementById('left-fighter-indicator');
  const secondHealthIndicator: HTMLElement = <HTMLElement>document.getElementById('right-fighter-indicator');

  // Copy object properties to don't mutate original object
  firstFighter = { ...firstFighter };
  secondFighter = { ...secondFighter };

  // Variables store the values of time, when fighter special ability was applied last time
  let lastTimeOfHitFirstPlayer: number = 0;
  let lastTimeOfHitSecondPlayer: number = 0;

  let isBlock1: boolean = false;
  let isBlock2: boolean = false;

  const fighterOneStarterHealth: number = firstFighter.health;
  const fighterTwoStarterHealth: number = secondFighter.health;

  // In this sets we add keys of special hits, which are being held by player
  const firstPlayerSpecialKeysPressed: Set<string> = new Set<string>();
  const secondPlayerSpecialKeysPressed: Set<string> = new Set<string>();

  return new Promise((resolve) => {

    document.addEventListener('keydown', handleKeydown);
    document.addEventListener('keyup', handleKeyup);

    function handleKeydown(event: KeyboardEvent): void {
      switch (event.code) {
        case controls.PlayerOneAttack:
          // if player holds block button, he is unable to atack or to be attacked
          if(!isBlock1 && !isBlock2){
            secondFighter.health -= getDamage(firstFighter, secondFighter);
          }
          break;
  
        case controls.PlayerTwoAttack:
          if(!isBlock1 && !isBlock2){
            firstFighter.health -= getDamage(secondFighter, firstFighter);
          }
          break;

        case controls.PlayerOneBlock:
          isBlock1 = true; 
          break;
        
        case controls.PlayerTwoBlock:
          isBlock2 = true;
          break;

        default:
          // if player does not hold a keys of block or atack, then handle case critical hit check 
          if (lastTimeOfHitFirstPlayer + SECOND * 10 < Date.now()) {
            if (controls.PlayerOneCriticalHitCombination.includes(event.code)) {
              firstPlayerSpecialKeysPressed.add(event.code);
            }
            // if player holds 3 of 3 keys needed to start critical hit
            if (firstPlayerSpecialKeysPressed.size === 3) {
              secondFighter.health -= 2 * firstFighter.attack;
              lastTimeOfHitFirstPlayer = Date.now();
              // after critical hit clear set to avoid keys sticking
              firstPlayerSpecialKeysPressed.clear();
            }
          }

          if (lastTimeOfHitSecondPlayer + SECOND * 10 < Date.now()) {
            if (controls.PlayerTwoCriticalHitCombination.includes(event.code)) {
              secondPlayerSpecialKeysPressed.add(event.code);
            };

            if(secondPlayerSpecialKeysPressed.size === 3) {
              firstFighter.health -= 2 * secondFighter.attack;
              lastTimeOfHitSecondPlayer = Date.now();
              secondPlayerSpecialKeysPressed.clear();
            }
          }
        }
        updateHealthIndicators();

        if(firstFighter.health <= 0){
          removeEventListenersFromDocument()
          resolve(secondFighter);
        } else if (secondFighter.health <= 0) {
          removeEventListenersFromDocument();
          resolve(firstFighter);
        }
    }
    
    function handleKeyup(event: KeyboardEvent): void {
      switch (event.code) {
        case controls.PlayerOneBlock:
          isBlock1 = false;
          break;
        
        case controls.PlayerTwoBlock:
          isBlock2 = false;
          break;

        default:
          setTimeout(()=>{
            firstPlayerSpecialKeysPressed.delete(event.code);
            secondPlayerSpecialKeysPressed.delete(event.code);
          }, 100)
          
      }
    }

    function removeEventListenersFromDocument(): void {
      document.removeEventListener('keydown', handleKeydown);
      document.removeEventListener('keyup', handleKeyup);
    }

    function updateHealthIndicators(): void {
      firstHealthIndicator.style.width = calculateHealthIndicatorWidth(fighterOneStarterHealth, firstFighter.health) + "%";
      secondHealthIndicator.style.width = calculateHealthIndicatorWidth(fighterTwoStarterHealth, secondFighter.health) + "%";
    }
  });
  
}

export function getDamage(attacker: Fighter, defender: Fighter): number {
  const hitPower: number = getHitPower(attacker);
  const blockPower: number = getBlockPower(defender);

  const damage: number = hitPower - blockPower;
  
  return damage > 0 ? damage: 0;
}

export function getHitPower(fighter: Fighter): number {
  const { attack } = fighter;
  const criticalHitChance: number = 1 + Math.random();

  const power: number = attack * criticalHitChance;

  return power;
}

export function getBlockPower(fighter: Fighter): number {
  const { defense } = fighter;
  const dodgeChance: number = 1 + Math.random();

  const power: number = defense * dodgeChance;

  return power; 
}

function calculateHealthIndicatorWidth(starterHealth: number, currentHealth: number): number {
  const width: number = 100 * currentHealth / starterHealth;
  return width < 0 ? 0: width;
}