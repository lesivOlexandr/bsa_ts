import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService'
import { Fighter } from '../models/fighter.model';

export interface selectFighterFunc {
  (event: MouseEvent, fighterId: string): Promise<void>
}

export function createFightersSelector(): selectFighterFunc {
  let selectedFighters: [Fighter?, Fighter?] = [];

  return async (event: MouseEvent, fighterId: string) => {
    const fighter: Fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter: Fighter | undefined = playerOne ?? fighter;
    const secondFighter: Fighter | undefined = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters as [Fighter, Fighter]);
  };
}

const fighterDetailsMap = new Map<string, Fighter>();

export async function getFighterInfo(fighterId: string): Promise<Fighter> {
  const cachedFighterDetail: Fighter | undefined = fighterDetailsMap.get(fighterId);
  if (cachedFighterDetail) {
    return cachedFighterDetail;
  } 

  const fighterDetails: Fighter = await fighterService.getFighterDetails(fighterId);
  fighterDetailsMap.set(fighterId, fighterDetails);

  return fighterDetails;
}

function renderSelectedFighters(selectedFighters: [Fighter, Fighter]): void {
  const fightersPreview: HTMLElement = <HTMLElement>document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview: HTMLDivElement = createFighterPreview(playerOne, 'left');
  const secondPreview: HTMLDivElement = createFighterPreview(playerTwo, 'right');
  const versusBlock: HTMLDivElement = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '' ;
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: [Fighter, Fighter]): HTMLDivElement {
  const canStartFight: boolean = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);

  const container: HTMLDivElement = createElement<HTMLDivElement>({
    tagName: 'div',
    className: 'preview-container___versus-block'
  });

  const image: HTMLImageElement = createElement<HTMLImageElement>({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });

  const disabledBtn: '' | 'disabled' = canStartFight ? '' : 'disabled';
  const fightBtn: HTMLButtonElement = createElement<HTMLButtonElement>({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: [Fighter, Fighter]): void {
  renderArena(selectedFighters);
}
