import { createElement } from '../helpers/domHelper';
import { Fighter } from '../models/fighter.model';

export function createFighterPreview(fighter: Fighter, position: 'left' | 'right'): HTMLDivElement {
  const positionClassName: string = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement: HTMLDivElement = createElement<HTMLDivElement>({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const imageElement: HTMLImageElement = createFighterImage(fighter);
    const fighterInformation: HTMLDivElement = createFighterDetails(fighter);

    fighterElement.append(imageElement, fighterInformation);
  }

  return fighterElement;
}

export function createFighterImage(fighter: Fighter): HTMLImageElement {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement: HTMLImageElement = createElement<HTMLImageElement>({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterDetails(fighter: Fighter): HTMLDivElement {
  const details: HTMLDivElement[] = Object.keys(fighter)
    .filter(key => key !== '_id' && key !== 'source')
    .map(key => {
      const detailElement: HTMLDivElement = createElement<HTMLDivElement>({ tagName: "div", className: "fighter-detail" });
      detailElement.innerText = key + ": " + fighter[key as keyof Fighter];
      return detailElement;
    });

  const detailsList: HTMLDivElement = createElement<HTMLDivElement>({ tagName: "div", className: "fighter-details" });
  detailsList.append(...details);
  return detailsList;
}
