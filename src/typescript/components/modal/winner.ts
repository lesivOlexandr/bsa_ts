import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import  App  from '../../app';
import { Fighter } from '../../models/fighter.model';

export function showWinnerModal(fighter: Fighter) {
  const bodyElement: HTMLDivElement = createModalBody(fighter);

  return showModal({ title: 'Congratulations', onClose: onModalClose, bodyElement });
}

function onModalClose(): App {
  const arenaElement = <HTMLElement>document.querySelector('.arena___root');
  arenaElement.remove();

  return new App();
}

function createModalBody(fighter: Fighter){
  const hintElement: HTMLSpanElement = createElement<HTMLSpanElement>({ tagName: 'span', className: 'modal__hint' });
  hintElement.innerText = `Fighter ${fighter.name} wins`;

  const bodyElement: HTMLDivElement = createElement<HTMLDivElement>({ tagName: 'div', className: 'modal__body' });
  bodyElement.append(hintElement);

  return bodyElement;
}
