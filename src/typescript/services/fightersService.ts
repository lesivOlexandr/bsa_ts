import { callApi } from '../helpers/apiHelper';
import { Fighter } from '../models/fighter.model';

class FighterService {
  async getFighters(): Promise<Partial<Fighter>[]> {
    try {
      const endpoint: string = 'fighters.json';
      const apiResult: Partial<Fighter>[] = await callApi<Partial<Fighter>[]>(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<Fighter> {
    try {
      const endpoint: string =  `details/fighter/${id}.json`;
      const apiResult: Fighter = await callApi<Fighter>(endpoint, 'GET');

      return apiResult
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
